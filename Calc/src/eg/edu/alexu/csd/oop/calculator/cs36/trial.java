package eg.edu.alexu.csd.oop.calculator.cs36;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.management.RuntimeErrorException;
import javax.swing.JOptionPane;

public class trial {
	private static double[] num=new double[2];
	private static double ans;
	private static String Text,finaltext;
	private static Deque<Double> before=new LinkedList<Double>();
	private static Deque<String> before2=new LinkedList<String>();
	private ListIterator<Double> j=(ListIterator<Double>) before.iterator();
	private ListIterator<String> k=(ListIterator<String>) before2.iterator();
	private int flag=0;
	public void intialize()
	{
		
		finaltext=null;
		before.clear();
		before2.clear();
	}
	public  void input(String s) {
		try {
			Text=s;
			String add1="-?[\\d.*]+[+]-?[\\d.*]+";
			String minus1="-?[\\d.]+[-]-?[\\d.*]+";
			String multi1="-?[\\d.]+[*]-?[\\d.*]+";
			String divide1="-?[\\d.*]+[/]-?[\\d.*]+";
			String numT="-?[\\d.]+";
			Pattern p=Pattern.compile(numT);
			Matcher m=p.matcher(Text);
			int i=0;
			ans=0.0;
		//	JOptionPane.showMessageDialog(null, Text);
			while(m.find())
			{
				num[i]=Double.parseDouble(m.group());
				i++;
			}
			if(Pattern.matches(add1, Text))
			{
				ans=num[0]+num[1];
			}
			else if(Pattern.matches(minus1, Text))
			{
				//JOptionPane.showMessageDialog(null, num[0]+num[1]);
				String s1="-?[\\d.]+[-][\\d.]+";
				if(Pattern.matches(s1, Text))
				{
					ans=num[0]+num[1];
				//	JOptionPane.showMessageDialog(null, num[0]+num[1]);
				}
				else
					ans=num[0]-num[1];
			}
			else if(Pattern.matches(multi1, Text))
			{
				//JOptionPane.showMessageDialog(null, num[0]*num[1]);
				ans=num[0]*num[1];
				
			}
			else if(Pattern.matches(divide1, Text))
			{
				ans=num[0]/num[1];
			}
			else
			{
				throw new RuntimeErrorException(null);
			}
			finaltext=Text;
			if(before.size()==5)
			{
				before.removeLast();
				before2.removeLast();
				before.push(ans);
				before2.push(Text);
			}
			else
			{
				before.push(ans);
				before2.push(Text);
			}
			j=(ListIterator<Double>)before.iterator();
			k=(ListIterator<String>)before2.iterator();
			flag=0;
			//return Double.toString(ans);
			}catch(Exception e1)
			{
				JOptionPane.showMessageDialog(null, "Invalid Input");
				//return null;
			}

	}

	public String getResult() {
		try {
			String add1="-?[\\d.*]+[+]-?[\\d.*]+";
			String minus1="-?[\\d.]+[-]-?[\\d.*]+";
			String multi1="-?[\\d.]+[*]-?[\\d.*]+";
			String divide1="-?[\\d.*]+[/]-?[\\d.*]+";
			String numT="-?[\\d.]+";
			Pattern p=Pattern.compile(numT);
			Matcher m=p.matcher(finaltext);
			int i=0;
			ans=0.0;
		//	JOptionPane.showMessageDialog(null, Text);
			while(m.find())
			{
				num[i]=Double.parseDouble(m.group());
				i++;
			}
			if(Pattern.matches(add1, finaltext))
			{
				ans=num[0]+num[1];
			}
			else if(Pattern.matches(minus1, finaltext))
			{
				//JOptionPane.showMessageDialog(null, num[0]+num[1]);
				String s="-?[\\d.]+[-][\\d.]+";
				if(Pattern.matches(s, finaltext))
				{
					ans=num[0]+num[1];
				//	JOptionPane.showMessageDialog(null, num[0]+num[1]);
				}
				else
					ans=num[0]-num[1];
			}
			else if(Pattern.matches(multi1, finaltext))
			{
				//JOptionPane.showMessageDialog(null, num[0]*num[1]);
				ans=num[0]*num[1];
				
			}
			else if(Pattern.matches(divide1, finaltext))
			{
				ans=num[0]/num[1];
			}
			else
			{
				throw new RuntimeErrorException(null);
			}
			
			return Double.toString(ans);
			}catch(Exception e1)
			{
				JOptionPane.showMessageDialog(null, "Invalid Input");
				return null;
			}
	}

	
	public String current() {
	//	System.out.println(finaltext);
		return finaltext;
	}
	public String prev() {
		try {
			
			if(flag==2)
			{
			
				String	a=(String)k.next();
				finaltext=a;
				return a;
			}
		if(flag==0)
		{	
			j.next();
			k.next();
			flag=3;
		}
		
		//j.next();
		String	a=(String)k.next();
		finaltext=a;
		return a;
		}catch(Exception e2)
		{	
			//JOptionPane.showMessageDialog(null, "Invalid");
			//k.previous();
			//k.previous();
			return null;
		}
	}

	
	public String next() {
		try {
			flag=1;
			if(k.hasPrevious())
			{
				k.previous();
			}
			else
			{
				
				return null;
			}
			if(k.hasPrevious())
			{
				k.previous();
			}
			else
			{
				k.next();
				return null;
			}
		String	a=(String)k.next();
			finaltext=a;
			//System.out.println("kjk");
			return a;
		}catch (Exception e2)
		{
			
			return null;
		}
		
	}

	
	public void save()  {
	     
	    BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter("C:\\Users\\Hagrass\\eclipse-workspace\\Calc\\samplefile1.txt"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    try {
	    	String a=Integer.toString(before.size());
	    	a+="\n";
	    	writer.write(a);
	    	a=finaltext;
	    	a+="\n";
	    	writer.write(a);
			Iterator<String> b = before2.descendingIterator();
			while(b.hasNext())
			{
				String s =(String)(b.next());
				s+="\n";
				writer.write(s);
			}
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "invalid");
			e.printStackTrace();
		}
	    try {
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "invalid");
			e.printStackTrace();
		}
		
	}

	
	public void load() {
		File file=new File("C:\\Users\\Hagrass\\eclipse-workspace\\Calc\\samplefile1.txt");
			try {
				BufferedReader br = new BufferedReader(new FileReader(file));
				 String st;
				 
				 try {
					 int i=0;
					 int size=0;
					while((st=br.readLine())!=null)
					 {
						if(i==0)
						{
							size=Integer.parseInt(st);
							i++;
							continue;
						}
						if(i==1)
						{
							
							finaltext=st;
							i++;
							continue;
						}
						if(before2.size()==5)
						{	
							before2.removeLast();
							before2.push(st);
						}
						else
						{
							before2.push(st);
						}
						
						i++;
					 }
					j=(ListIterator<Double>)before.iterator();
					k=(ListIterator<String>)before2.iterator();
					//JOptionPane.showMessageDialog(null, (double)j.next());
				} catch (IOException e) {
					JOptionPane.showMessageDialog(null, "invalid");
					e.printStackTrace();
				}
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				JOptionPane.showMessageDialog(null, "invalid");
				e.printStackTrace();
			}
	}
	

}
