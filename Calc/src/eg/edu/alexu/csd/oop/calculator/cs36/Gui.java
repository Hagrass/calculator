package eg.edu.alexu.csd.oop.calculator.cs36;

import java.awt.EventQueue;


import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.xml.transform.TransformerException;
import javax.management.RuntimeErrorException;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.awt.event.ActionEvent;

public class Gui implements Calculator {

	private JFrame frame;
	private JTextField textField;
	private static double[] num=new double[2];
	private static double ans;
	private static String Text,finaltext=null;
	private static Deque<String> before=new LinkedList<String>();
	private static Deque<String> before2=new LinkedList<String>();
	private ListIterator<String> j=(ListIterator<String>) before.iterator();
	private ListIterator<String> k=(ListIterator<String>) before2.iterator();
	private JTextField Textfieldans;
	private int flag=0;
	private JButton btnNewButton;
	private JButton btnNewButton_1;
	private JButton btnLoad;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui window = new Gui();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Gui() {
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(43, 35, 96, 19);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		Text=null;
		finaltext=null;
		before.clear();
		before2.clear();
		
		JButton btnNewbutton = new JButton("Answer");
		btnNewbutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				input(textField.getText());
				Textfieldans.setText(getResult());
				//JOptionPane.showMessageDialog(null,finaltext);
				
				
			}
		});
		btnNewbutton.setBounds(43, 186, 85, 21);
		frame.getContentPane().add(btnNewbutton);
		
		Textfieldans = new JTextField();
		Textfieldans.setBounds(183, 187, 96, 19);
		frame.getContentPane().add(Textfieldans);
		Textfieldans.setColumns(10);
		
		btnNewButton = new JButton("prev");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try
				{
				String b=prev(); 
				if(b!=null)
				{
					//String a=(String)j.next();
					textField.setText(b);
					
					Textfieldans.setText(finalprev);
				}
				
				//String a=Double.toString((double)j.next());
				//JOptionPane.showMessageDialog(null,a);
				//Textfieldans.setText(a);
				
				//j.next();
				}catch(Exception e1)
				{
					JOptionPane.showMessageDialog(null,"invalid");
				}
			}
		});
		btnNewButton.setBounds(256, 216, 85, 21);
		frame.getContentPane().add(btnNewButton);
		
		btnNewButton_1 = new JButton("next");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try
				{
				String b=next(); 
				if(b!=null)
				{
					textField.setText(b);
					String a=finalnext;
					Textfieldans.setText(a);
				}
				//
				
				//j.next();
				}catch(Exception e1)
				{
					JOptionPane.showMessageDialog(null,"invalid");
				}
			}
		});
		btnNewButton_1.setBounds(341, 216, 85, 21);
		frame.getContentPane().add(btnNewButton_1);
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				save();
			}
		});
		btnSave.setBounds(341, 10, 85, 21);
		frame.getContentPane().add(btnSave);
		
		btnLoad = new JButton("load");
		btnLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				load();
				JOptionPane.showMessageDialog(null, finaltext);
				flag=2;
			}
		});
		btnLoad.setBounds(341, 57, 85, 21);
		frame.getContentPane().add(btnLoad);
	}

	@Override
	public void input(String s) {
		try {
			Text=s;
			String add1="-?[\\d.*]+[+]-?[\\d.*]+";
			String minus1="-?[\\d.]+[-]-?[\\d.*]+";
			String multi1="-?[\\d.]+[*]-?[\\d.*]+";
			String divide1="-?[\\d.*]+[/]-?[\\d.*]+";
			String numT="-?[\\d.]+";
			Pattern p=Pattern.compile(numT);
			Matcher m=p.matcher(Text);
			int i=0;
			ans=0.0;
		//	JOptionPane.showMessageDialog(null, Text);
			while(m.find())
			{
				num[i]=Double.parseDouble(m.group());
				i++;
			}
			if(Pattern.matches(add1, Text))
			{
				ans=num[0]+num[1];
			}
			else if(Pattern.matches(minus1, Text))
			{
				//JOptionPane.showMessageDialog(null, num[0]+num[1]);
				String s1="-?[\\d.]+[-][\\d.]+";
				if(Pattern.matches(s1, Text))
				{
					ans=num[0]+num[1];
				//	JOptionPane.showMessageDialog(null, num[0]+num[1]);
				}
				else
					ans=num[0]-num[1];
			}
			else if(Pattern.matches(multi1, Text))
			{
				//JOptionPane.showMessageDialog(null, num[0]*num[1]);
				ans=num[0]*num[1];
				
			}
			else if(Pattern.matches(divide1, Text))
			{
				ans=num[0]/num[1];
			}
			else
			{
				throw new RuntimeErrorException(null);
			}
			finaltext=Text;
			if(before.size()==5)
			{
				before.removeLast();
				before2.removeLast();
				before.push(Double.toString(ans));
				before2.push(Text);
			}
			else
			{
				before.push(Double.toString(ans));
				before2.push(Text);
			}
			j=(ListIterator<String>)before.iterator();
			k=(ListIterator<String>)before2.iterator();
			flag=0;
			//return Double.toString(ans);
			}catch(Exception e1)
			{
				JOptionPane.showMessageDialog(null, "Invalid Input");
				//return null;
			}

	}
	@Override
	public String getResult() {
		try {
			String add1="-?[\\d.*]+[+]-?[\\d.*]+";
			String minus1="-?[\\d.]+[-]-?[\\d.*]+";
			String multi1="-?[\\d.]+[*]-?[\\d.*]+";
			String divide1="-?[\\d.*]+[/]-?[\\d.*]+";
			String numT="-?[\\d.]+";
			Pattern p=Pattern.compile(numT);
			Matcher m=p.matcher(finaltext);
			int i=0;
			ans=0.0;
		//	JOptionPane.showMessageDialog(null, Text);
			while(m.find())
			{
				num[i]=Double.parseDouble(m.group());
				i++;
			}
			if(Pattern.matches(add1, finaltext))
			{
				ans=num[0]+num[1];
			}
			else if(Pattern.matches(minus1, finaltext))
			{
				//JOptionPane.showMessageDialog(null, num[0]+num[1]);
				String s="-?[\\d.]+[-][\\d.]+";
				if(Pattern.matches(s, Text))
				{
					ans=num[0]+num[1];
				//	JOptionPane.showMessageDialog(null, num[0]+num[1]);
				}
				else
					ans=num[0]-num[1];
			}
			else if(Pattern.matches(multi1, finaltext))
			{
				//JOptionPane.showMessageDialog(null, num[0]*num[1]);
				ans=num[0]*num[1];
				
			}
			else if(Pattern.matches(divide1, finaltext))
			{
				ans=num[0]/num[1];
			}
			else
			{
				throw new RuntimeErrorException(null);
			}
			
			return Double.toString(ans);
			}catch(Exception e1)
			{
				JOptionPane.showMessageDialog(null, "Invalid Input");
				return null;
			}
	}

	@Override
	public String current() {
		// TODO Auto-generated method stub
		if(flag==-1)
		{
			return null;
		}
		//JOptionPane.showMessageDialog(null, before2);
		return finaltext;
	}
	
	private static String finalprev;
	@Override
	public String prev() {
		try {
			String b;
			String	a;
			if(flag==2)
			{
				 b=	(String)j.next();
					a=(String)k.next();
				finaltext=a;
				finalprev=b;
				return a;
			}
		if(flag==0)
		{	
			 b=	(String)j.next();
				a=(String)k.next();
			flag=3;
		}
		b=j.next();
		a=(String)k.next();
		finaltext=a;
		finalprev=b;
		return a;
		}catch(Exception e2)
		{	
			JOptionPane.showMessageDialog(null, "Invalid");
			//k.next();
			//k.previous();
			return null;
		}
	}
	private String finalnext;
	@Override
	
	public String next() {
		try {
			flag=1;
			String a,b;
			if(k.hasPrevious())
			{
				a=k.previous();
				b=j.previous();
			}
			else
			{
				JOptionPane.showMessageDialog(null, "Invalid");
				return null;
			}
			if(k.hasPrevious())
			{
				
				a=k.previous();
				b=j.previous();
			}
			else
			{
				a=k.next();
				b=j.next();
				JOptionPane.showMessageDialog(null, "Invalid");
				return null;
			}
			b=j.next();
			a=(String)k.next();
			finaltext=a;
			finalnext=b;
			return a;
		}catch (Exception e2)
		{
			//JOptionPane.showMessageDialog(null, "Invalid");
			return null;
		}
		
	}

	@Override
	public void save()  {
	     
	    BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter("samplefile1.txt"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    try {
	    	
	    	String a=Integer.toString(before.size());
	    	a+="\n";
	    	writer.write(a);
	    	a=finaltext;
	    	a+="\n";
	    	writer.write(a);
	    	Iterator<String> b = before2.descendingIterator();
	    	Iterator<String> b2 = before.descendingIterator();
			while(b.hasNext())
			{
				String s =(String)(b.next());
				s+="\n";
				writer.write(s);
				s=(String)(b2.next());
				s+="\n";
				writer.write(s);
			}
		
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "invalid");
			e.printStackTrace();
		}
	    try {
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "invalid");
			e.printStackTrace();
		}
		
	}

	@Override
	public void load() {
		File file=new File("samplefile1.txt");
			try {
				BufferedReader br = new BufferedReader(new FileReader(file));
				 String st;
				 try {
					 int i=0;
					 int size=0;
					finaltext="";
					while((st=br.readLine())!=null)
					 {
						if(i==0)
						{
							size=Integer.parseInt(st);
							i++;
							continue;
						}
						if(i==1)
						{
							
							finaltext=st;
							i++;
							continue;
						}
						if(i%2==0)
						{
						if(before2.size()==5)
						{	
							before2.removeLast();
							before2.push(st);
							
						}
						else
						{
							before2.push(st);
						}
						}
						else
						{
							if(before.size()==5)
							{	
								before.removeLast();
								before.push(st);
								
							}
							else
							{
								before.push(st);
							}
						}
						i++;
					 }
					if(i==2)
						flag=-1;
					//JOptionPane.showMessageDialog(null, i);
					j=(ListIterator<String>)before.iterator();
					k=(ListIterator<String>)before2.iterator();
					//JOptionPane.showMessageDialog(null, (double)j.next());
				} catch (IOException e) {
					JOptionPane.showMessageDialog(null, "invalid");
					e.printStackTrace();
				}
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				JOptionPane.showMessageDialog(null, "invalid");
				e.printStackTrace();
			}
		
		
		
		
		
	}
}
